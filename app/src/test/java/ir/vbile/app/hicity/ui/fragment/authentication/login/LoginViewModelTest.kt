package ir.vbile.app.hicity.ui.fragment.authentication.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import io.kotest.matchers.shouldBe
import ir.vbile.app.hicity.ui.fragment.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class LoginViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var vm: LoginViewModel

    @Before
    fun setUp() {
        vm = LoginViewModel(/**authRepository**/)
    }

    @Test
    fun `empty mobile number, return MobileNumberIsEmpty`() = runBlockingTest {
        vm.mobileNumber.value = ""
        vm.validateMobileNumber()
        vm.loginEvent.test {
            expectItem() shouldBe LoginViewModel.LoginEvent.MobileNumberIsEmpty
        }
    }

    @Test
    fun `invalid mobile number, returns InvalidPhoneNumber`() = runBlockingTest {
        vm.mobileNumber.value = "646546"
        vm.validateMobileNumber()
        vm.loginEvent.test {
            expectItem() shouldBe LoginViewModel.LoginEvent.InvalidPhoneNumber
        }
    }

    @Test
    fun `less than 11 digits, returns InvalidPhoneNumber`() = runBlockingTest {
        vm.mobileNumber.value = "0910299927"
        vm.validateMobileNumber()
        vm.loginEvent.test {
            expectItem() shouldBe LoginViewModel.LoginEvent.InvalidPhoneNumber
        }
    }

    @Test
    fun `valid mobile number, returns NavigateToVerifyOtpScreen`() = runBlockingTest {
        vm.mobileNumber.value = "+989102999274"
        vm.validateMobileNumber()
        vm.loginEvent.test {
            expectItem() shouldBe LoginViewModel.LoginEvent.NavigateToVerifyOtpScreen("+989102999274")
        }
    }
}