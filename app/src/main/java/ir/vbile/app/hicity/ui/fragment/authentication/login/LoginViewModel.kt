package ir.vbile.app.hicity.ui.fragment.authentication.login

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hicity.base.BaseViewModel
import ir.vbile.app.hicity.extentions.isValidPhoneNumber
import ir.vbile.app.hicity.wrapper.FailureInterface
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    val mainCoroutineDispatcher: CoroutineDispatcher,
    ) : BaseViewModel(mainCoroutineDispatcher) {
    val mobileNumber: MutableStateFlow<String> = MutableStateFlow("")

    /**
     * the mobile number is not valid if
     * ... the mobile number is empty
     * ... the mobile number is not an valid mobile number
     * ... the mobile number contains less than 11 digits
     */
    fun validateMobileNumber() {
        val mobileNumber = mobileNumber.value
        doInMain {
            if (mobileNumber.isEmpty()) {
                loginEventChannel.send(LoginEvent.MobileNumberIsEmpty)
                return@doInMain
            }
            if (!mobileNumber.isValidPhoneNumber()) {
                loginEventChannel.send(LoginEvent.InvalidPhoneNumber)
                return@doInMain
            }
            if (mobileNumber.count { it.isDigit() } < 12) {
                loginEventChannel.send(LoginEvent.InvalidPhoneNumber)
                return@doInMain
            }
            sendOtp(mobileNumber)
            return@doInMain
        }
    }

    private fun sendOtp(mobileNumber: String) = viewModelScope.launch {
        // FIXME: 7/2/2021  
//        val result = authRepository.sendOtp(mobileNumber)
//        if (result.success == true) {
//            loginEventChannel.send(LoginEvent.NavigateToVerifyOtpScreen(mobileNumber))
//            return@launch
//        } else {
//            loginEventChannel.send(LoginEvent.ShowError(result.failure))
//            return@launch
//        }
    }

    private val loginEventChannel = Channel<LoginEvent>()
    val loginEvent = loginEventChannel.receiveAsFlow()

    sealed class LoginEvent {
        object MobileNumberIsEmpty : LoginEvent()
        object InvalidPhoneNumber : LoginEvent()
        data class NavigateToVerifyOtpScreen(val mobileNumber: String) : LoginEvent()
        data class ShowError(val failure: FailureInterface?) : LoginEvent()
    }
}