package ir.vbile.app.hicity.data.factory

import ir.vbile.app.hicity.data.model.*
import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.profile.ResponseFeedApiProfile
import kotlin.random.*

object DummyProfileApiResponse {

    val asgharProfile = User(
        id = 1001,
        fullName = "سید اصغر حسینی",
        genderId = 1001,
        avatarUrl = "https://thumb.jobinjacdn.com/s9_ce7Y7FKPE_sYHrR5u58iHuME=/256x256/filters:strip_exif():format(jpeg)/https://storage.jobinjacdn.com/other/js_avatar_image_blob/5515de93-d4e0-4097-9add-485f669477e8/1_main.png",
        totalIssue = Random.nextInt(10, 100),
        totalVote = Random.nextInt(10, 100),
        totalComment = Random.nextInt(10, 100),
        aboutUser = "سلام سید اصغر حسینی هستم."
    )
    val vahidProfile = User(
        id = 1002,
        fullName = "وحید گروسی",
        genderId = 1002,
        avatarUrl = "https://s4.uupload.ir/files/19606714_y6v.jpg",
        totalIssue = Random.nextInt(10, 100),
        totalVote = Random.nextInt(10, 100),
        totalComment = Random.nextInt(10, 100),
        aboutUser = "سلام وحید گروسی هستم."
    )
    val pejmanProfile = User(
        id = 1003,
        fullName = "پژمان آزاد رنجبر",
        genderId = 1003,
        avatarUrl = "https://s4.uupload.ir/files/40461630_77g7.jpg",
        totalIssue = Random.nextInt(10, 100),
        totalVote = Random.nextInt(10, 100),
        totalComment = Random.nextInt(10, 100),
        aboutUser = "سلام پژمان آزاد رنجبرهستم."
    )

    val userList = arrayListOf<User>(asgharProfile, vahidProfile, pejmanProfile)

    fun getUser(id: Int): User {
        var user: User? = null
        userList.forEach {
            if (it.id == id) {
                user = it
            }
        }
        return user!!
    }
    val dummyUser =
        User(1, "پژمان رنجبر", 1, "https://avatars.githubusercontent.com/u/40461630",54,54,54,"")
    val dummyTime = "2021-07-02T10:50:41Z"
    val dummyLocation = Location("",35.9726, 50.6068)
    val issuePhotos = listOf(
        "https://www.dana.ir/content/files/Parsiankhabar/1070464.jpg",
        "https://www.dana.ir/content/files/Parsiankhabar/1070465.jpg",
        "https://www.dana.ir/content/files/Parsiankhabar/1070467.jpg"
    )
    val dummyCategory = Category(1, "راه و شهرسازی")
    val dummySubCategory = SubCategory(1, " لکه گیری چاله های معابر", 1)
    val dummyDescription =
        "کوچه\u200Cها و خیابان\u200Cهای پر چاله\u200Cچوله نه\u200Cتنها موجب آزار و اذیت شهروندان می\u200Cشود بلکه زیبنده شهر نیست و اگر مسافر و یا گردشگری از شهر عبور کند در نگاه اول با خیابان\u200Cهای فرسوده مواجه می\u200Cشود."

    val dummyComments = listOf(
        Comment(1, dummyUser, "1", true, true, dummyTime, true,100 ),
        Comment(2, dummyUser, "1", true, true, dummyTime, false,50 ),
        Comment(3, dummyUser, "1", true, true, dummyTime, false,0 )
    )
    val dummyIssues = (1..5).map {
        Issue(
            it,
            dummyUser,
            dummyTime,
            dummyLocation,
            issuePhotos,
            true,
            dummyCategory,
            dummySubCategory,
            dummyDescription,
            dummyComments
        )
    }

    val dummyFeedApiResponse = ResponseFeedApiProfile(dummyIssues, 1, 5, 1)

}