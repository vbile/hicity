package ir.vbile.app.hicity

import android.app.*
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import dagger.hilt.android.*
import timber.log.Timber

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}