package ir.vbile.app.hicity.data.repo.impl

import ir.vbile.app.hicity.data.remote.api.FeedApi
import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.hicity.data.repo.FeedRepository
import ir.vbile.app.hicity.util.safeApiCall
import ir.vbile.app.hicity.wrapper.Resource

class FeedRepositoryImpl(private val feedApi: FeedApi) : FeedRepository {

    override suspend fun feed(page: Int, apiKey: String): Resource<FeedApiResponse> =
        safeApiCall { feedApi.feed(page, apiKey) }

}
