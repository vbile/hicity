package ir.vbile.app.hicity.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.vbile.app.hicity.data.remote.api.AuthApi
import ir.vbile.app.hicity.data.remote.api.FeedApi
import ir.vbile.app.hicity.data.remote.api.ProfileApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RestClientModule {

    @Provides
    @Singleton
    internal fun provideFeedApiService(
        @HiCityRetrofit retrofit: Retrofit
    ): FeedApi = retrofit.create(FeedApi::class.java)

    @Provides
    @Singleton
    internal fun provideAuthApiService(
        @HiCityRetrofit retrofit: Retrofit
    ): AuthApi = retrofit.create(AuthApi::class.java)
    @Provides
    @Singleton
    internal fun provideProfileApiService(
        @HiCityRetrofit retrofit: Retrofit
    ): ProfileApi = retrofit.create(ProfileApi::class.java)

    @Provides
    @HiCityRetrofit
    @Singleton
    internal fun provideHiCityRetrofit(
        okHttpClient: OkHttpClient,
        moshi: Moshi
    ): Retrofit = Retrofit.Builder()
        .baseUrl(EndPoints.STAGE_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
}

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class HiCityRetrofit