package ir.vbile.app.hicity.data.repo.dummy

import ir.vbile.app.hicity.data.factory.DummyObjects.dummyFeedApiResponse
import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.hicity.data.repo.FeedRepository
import ir.vbile.app.hicity.wrapper.Resource

class DummyFeedRepository : FeedRepository {

    override suspend fun feed(page: Int, apiKey: String): Resource<FeedApiResponse> =
        Resource.Success(dummyFeedApiResponse)

}