package ir.vbile.app.hicity.ui.fragment.search

import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hicity.base.BaseViewModel
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    val mainCoroutineDispatcher: CoroutineDispatcher,
) : BaseViewModel(mainCoroutineDispatcher) {
}