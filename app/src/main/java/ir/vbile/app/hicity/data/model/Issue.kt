package ir.vbile.app.hicity.data.model

import com.squareup.moshi.Json
import ir.vbile.app.hicity.data.model.share.*

data class Issue(
    @Json(name = "id")
    val id: Int,
    @Json(name = "user")
    val user: User,
    @Json(name = "cratedAt")
    val cratedAt: String,
    @Json(name = "location")
    val location: Location,
    @Json(name = "photosUrl")
    val photosUrl: List<String>,
    @Json(name = "isAgree")
    val isAgree: Boolean?,
    @Json(name = "category")
    val category: Category,
    @Json(name = "subCategory")
    val subCategory: SubCategory,
    @Json(name = "description")
    val description: String,
    @Json(name = "comments")
    val comments: List<Comment>
)