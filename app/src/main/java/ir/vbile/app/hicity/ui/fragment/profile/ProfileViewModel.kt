package ir.vbile.app.hicity.ui.fragment.profile

import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hicity.base.BaseViewModel
import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.profile.ResponseFeedApiProfile
import ir.vbile.app.hicity.data.repo.ProfileRepository
import ir.vbile.app.hicity.di.EndPoints.fakeApiKey
import ir.vbile.app.hicity.wrapper.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    val mainCoroutineDispatcher: CoroutineDispatcher,
    private val repository: ProfileRepository
) : BaseViewModel(mainCoroutineDispatcher) {

    private val userEventChannel = Channel<Resource<User>>()
    val userEvent = userEventChannel.receiveAsFlow()

    init {
        getFeedProfile(1)
        getUser(1001)
    }

     fun getUser(userId: Int) = doInMain {
        userEventChannel.send(Resource.Loading)
        userEventChannel.send(repository.getUser(userId))
    }

    private val feedProfileChanel = Channel<Resource<ResponseFeedApiProfile>>()
    val feedEvent = feedProfileChanel.receiveAsFlow()

    fun getFeedProfile(page: Int) = doInMain {
        feedProfileChanel.send(Resource.Loading)
        feedProfileChanel.send(repository.getUserFeedProfile(page, fakeApiKey))
    }
}