package ir.vbile.app.hicity.di

import ir.vbile.app.hicity.BuildConfig

object EndPoints {
    const val STAGE_BASE_URL = BuildConfig.STAGE_BASE_URL
    const val API_PRODUCTION_URL = BuildConfig.API_PRODUCTION_URL

    const val loginWithMobileNumber = "auth/login"
    const val sendVerificationCode = "auth/verification-code/send"
    const val feedRequest = "feed/"
    const val fakeApiKey = "Fake api key"
    const val itemsCountPerPage = 10
    const val profileRequest = ""
    const val profileFeedRequest = ""
}