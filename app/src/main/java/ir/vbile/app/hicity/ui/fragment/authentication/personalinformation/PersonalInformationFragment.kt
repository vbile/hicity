package ir.vbile.app.hicity.ui.fragment.authentication.personalinformation

import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment
import ir.vbile.app.hicity.databinding.*
import ir.vbile.app.hicity.other.viewBinding

@AndroidEntryPoint
class PersonalInformationFragment : BaseFragment<PersonalInformationViewModel>(
    R.layout.fragment_personal_information,
    PersonalInformationViewModel::class
) {
    private val binding by viewBinding(FragmentLoginBinding::bind)
    override fun setUpViews() {
        super.setUpViews()
    }

    override fun observeData() {
        super.observeData()
    }
}