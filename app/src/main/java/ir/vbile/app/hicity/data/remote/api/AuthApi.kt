package ir.vbile.app.hicity.data.remote.api

import ir.vbile.app.hicity.data.remote.responses.authentication.LoginResponse
import ir.vbile.app.hicity.data.remote.responses.authentication.SendOtpResponse
import ir.vbile.app.hicity.di.EndPoints
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthApi {
    @POST(EndPoints.loginWithMobileNumber)
    @FormUrlEncoded
    suspend fun login(
        @Field("mobileNumber") mobileNumber: String,
        @Field("code") code: String
    ): Response<LoginResponse>

    @POST(EndPoints.sendVerificationCode)
    @FormUrlEncoded
    suspend fun sendOtp(
        @Field("mobileNumber") mobileNumber: String
    ): Response<SendOtpResponse>
}