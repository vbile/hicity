package ir.vbile.app.hicity.data.model.share

import com.squareup.moshi.Json

data class Comment(
    @Json(name = "id")
    val id: Int,
    @Json(name = "user")
    val user: User,
    @Json(name = "description")
    val Comment: String,
    @Json(name = "isPublished")
    val isPublished: Boolean,
    @Json(name = "isApproved")
    val isApproved: Boolean,
    @Json(name = "cratedAt")
    val cratedAt: String,
    @Json(name = "isLiked")
    val isLiked: Boolean,
    @Json(name = "likeCount")
    val likeCount: Int
)