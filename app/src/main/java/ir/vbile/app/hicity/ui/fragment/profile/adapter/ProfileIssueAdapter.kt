package ir.vbile.app.hicity.ui.fragment.profile.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.databinding.*
import ir.vbile.app.hicity.ui.fragment.profile.listener.*
import ir.vbile.app.hicity.ui.fragment.profile.viewholder.*


class ProfileIssueAdapter(private val issueClickListener: ProfileIssueClickListener? = null) :
    ListAdapter<Issue, ProfileIssueItemViewHolder>(IssueDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileIssueItemViewHolder {
        return ProfileIssueItemViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: ProfileIssueItemViewHolder, position: Int) {
        val issue = getItem(position)
        holder.bind(issue)
        setListeners(holder.itemProfileIssueBinding, issue, position)
        holder.itemView.setOnClickListener {
            onItemClickListener?.let { click ->
                click(issue)
            }
        }
    }

    private fun setListeners(
        itemProfileIssueBinding: ItemProfileIssueBinding,
        issue: Issue,
        position: Int
    ) {
        with(itemProfileIssueBinding) {
            btnItemCheckTheStatus.setOnClickListener {
                issueClickListener?.onCheckTheStatusClick(issue)
            }

        }
    }

    private var onItemClickListener: ((Issue) -> Unit)? = null
    fun setOnItemClickListener(listener: (Issue) -> Unit) {
        onItemClickListener = listener
    }
}

class IssueDiffUtil : DiffUtil.ItemCallback<Issue>() {
    override fun areItemsTheSame(oldItem: Issue, newItem: Issue): Boolean = oldItem.id == oldItem.id
    override fun areContentsTheSame(oldItem: Issue, newItem: Issue): Boolean = oldItem == oldItem
}