package ir.vbile.app.hicity.ui.fragment.authentication.verificationcode

import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment
import ir.vbile.app.hicity.databinding.*
import ir.vbile.app.hicity.other.viewBinding

@AndroidEntryPoint
class VerificationCodeFragment : BaseFragment<VerificationCodeViewModel>(R.layout.fragment_verification_code, VerificationCodeViewModel::class) {
    private val binding by viewBinding(FragmentLoginBinding::bind)
    override fun setUpViews() {
        super.setUpViews()

    }

    override fun observeData() {
        super.observeData()
    }
}