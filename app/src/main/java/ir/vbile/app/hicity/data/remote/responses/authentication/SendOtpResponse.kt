package ir.vbile.app.hicity.data.remote.responses.authentication

import com.squareup.moshi.Json

data class SendOtpResponse(
    @Json(name = "isSuccess")
    val isSuccess: Boolean
)
