package ir.vbile.app.hicity.ui.fragment.home.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hicity.ui.fragment.home.viewholder.IssueImageMediaViewHolder

class IssueMediaAdapter : ListAdapter<String, IssueImageMediaViewHolder>(StringDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueImageMediaViewHolder {
        return IssueImageMediaViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: IssueImageMediaViewHolder, position: Int) {
        val url = getItem(position)
        holder.bind(url)
    }


}

class StringDiffUtil : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem
}