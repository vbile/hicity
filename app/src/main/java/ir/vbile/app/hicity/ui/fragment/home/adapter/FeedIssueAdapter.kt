package ir.vbile.app.hicity.ui.fragment.home.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.databinding.IssueItemBinding
import ir.vbile.app.hicity.ui.fragment.home.listener.IssueClickListener
import ir.vbile.app.hicity.ui.fragment.home.viewholder.IssueItemViewHolder

class FeedIssueAdapter(
    private val currentUserAvatarUrl: String,
    private val issueClickListener: IssueClickListener? = null
) :
    ListAdapter<Issue, IssueItemViewHolder>(IssueDiffUtil()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueItemViewHolder {
        return IssueItemViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: IssueItemViewHolder, position: Int) {
        val issue = getItem(position)
        holder.bind(currentUserAvatarUrl, issue)
        setListeners(holder.issueItemBinding, issue, position)
    }

    private fun setListeners(issueItemBinding: IssueItemBinding, issue: Issue, position: Int) {
        with(issueItemBinding) {
            ivAvatar.setOnClickListener {
                issueClickListener?.onAvatarClick(issue.user)
            }
            ivMore.setOnClickListener {
                issueClickListener?.onMoreItemClick(issue)
            }
            btnLocation.setOnClickListener {
                issueClickListener?.onLocationClick(issue.location)
            }
            btnDislike.setOnClickListener {
                issueClickListener?.onDislikeClick(issue)
            }
            btnLike.setOnClickListener {
                issueClickListener?.onLikeClick(issue)
            }
            tvShowAll.setOnClickListener {
                issueClickListener?.onShowAllCommentsClick(issue)
            }

            previewComment.ivLike.setOnClickListener {
                issueClickListener?.onCommentLikeClick(issue.comments.first())
            }
            previewComment.tvReplay.setOnClickListener {
                issueClickListener?.onCommentReplayClick(issue.comments.first())
            }
            previewComment.ivCommentAvatar.setOnClickListener {
                issueClickListener?.onCommentUserAvatarClick(issue.comments.first().user)
            }

            ivUserComment.setOnClickListener {
                issueClickListener?.onSendCommentClick(issue)
            }

        }
    }

}

class IssueDiffUtil : DiffUtil.ItemCallback<Issue>() {
    override fun areItemsTheSame(oldItem: Issue, newItem: Issue): Boolean = oldItem.id == oldItem.id
    override fun areContentsTheSame(oldItem: Issue, newItem: Issue): Boolean = oldItem == oldItem
}