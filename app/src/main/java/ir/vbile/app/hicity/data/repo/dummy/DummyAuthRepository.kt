package ir.vbile.app.hicity.data.repo.dummy

import ir.vbile.app.hicity.data.remote.responses.authentication.LoginResponse
import ir.vbile.app.hicity.data.remote.responses.authentication.SendOtpResponse
import ir.vbile.app.hicity.data.repo.AuthRepository
import ir.vbile.app.hicity.wrapper.Resource

class DummyAuthRepository : AuthRepository {
    override suspend fun login(mobileNumber: String, code: String): Resource<LoginResponse> =
        Resource.Success(LoginResponse(true, "AAAAA"))

    override suspend fun sendOtp(mobileNumber: String): Resource<SendOtpResponse> =
        Resource.Success(SendOtpResponse(true))
}