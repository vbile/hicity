package ir.vbile.app.hicity.data.remote.api

import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.hicity.di.EndPoints
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface FeedApi {
    @POST(EndPoints.feedRequest)
    @FormUrlEncoded
    suspend fun feed(
        @Field("page")
        page: Int,
        @Field("apiKey")
        apiKey: String,
        @Field("itemsCountPerPage")
        pageSize: Int = EndPoints.itemsCountPerPage
    ): Response<FeedApiResponse>
}