package ir.vbile.app.hicity.ui.fragment.news

import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment

@AndroidEntryPoint
class NewsFragment : BaseFragment<NewsViewModel>(R.layout.fragment_news, NewsViewModel::class) {

}