package ir.vbile.app.hicity.data.remote.responses.authentication

import com.squareup.moshi.Json

data class LoginResponse(
    @Json(name = "isSuccess")
    val isSuccess: Boolean,
    @Json(name = "apiKey")
    val apiKey: String
)
