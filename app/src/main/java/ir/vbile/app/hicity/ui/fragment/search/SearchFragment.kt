package ir.vbile.app.hicity.ui.fragment.search

import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment

@AndroidEntryPoint
class SearchFragment:BaseFragment<SearchViewModel>(R.layout.fragment_search,SearchViewModel::class) {

}