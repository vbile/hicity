package ir.vbile.app.hicity.data.repo

import ir.vbile.app.hicity.data.remote.responses.authentication.LoginResponse
import ir.vbile.app.hicity.data.remote.responses.authentication.SendOtpResponse
import ir.vbile.app.hicity.wrapper.Resource

interface AuthRepository {
    suspend fun login(mobileNumber: String, code: String): Resource<LoginResponse>
    suspend fun sendOtp(mobileNumber: String): Resource<SendOtpResponse>
}