package ir.vbile.app.hicity.data.repo.impl

import ir.vbile.app.hicity.data.remote.api.AuthApi
import ir.vbile.app.hicity.data.remote.responses.authentication.LoginResponse
import ir.vbile.app.hicity.data.remote.responses.authentication.SendOtpResponse
import ir.vbile.app.hicity.data.repo.AuthRepository
import ir.vbile.app.hicity.util.safeApiCall
import ir.vbile.app.hicity.wrapper.Resource

class AuthRepositoryImpl(private val authApi: AuthApi) : AuthRepository {

    override suspend fun login(mobileNumber: String, code: String): Resource<LoginResponse> =
        safeApiCall { authApi.login(mobileNumber, code) }

    override suspend fun sendOtp(mobileNumber: String): Resource<SendOtpResponse> =
        safeApiCall { authApi.sendOtp(mobileNumber) }
}