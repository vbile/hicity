package ir.vbile.app.hicity.ui.fragment.home

import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment
import ir.vbile.app.hicity.data.factory.DummyObjects.dummyUser
import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.data.model.share.Comment
import ir.vbile.app.hicity.data.model.share.Location
import ir.vbile.app.hicity.data.model.share.User
import ir.vbile.app.hicity.databinding.FragmentHomeBinding
import ir.vbile.app.hicity.other.viewBinding
import ir.vbile.app.hicity.ui.fragment.home.adapter.FeedIssueAdapter
import ir.vbile.app.hicity.ui.fragment.home.listener.IssueClickListener
import ir.vbile.app.hicity.util.hideKeyboard
import ir.vbile.app.hicity.util.showKeyboard
import ir.vbile.app.hicity.wrapper.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeViewModel>(R.layout.fragment_home, HomeViewModel::class),
    IssueClickListener {

    private val binding by viewBinding(FragmentHomeBinding::bind)
    private val feedIssueAdapter by lazy { FeedIssueAdapter(dummyUser.avatarUrl!!, this) }


    override fun setUpViews() {
        super.setUpViews()

        Glide.with(requireView()).load(dummyUser.avatarUrl)
            .into(binding.feedCommentBox.ivUserAvatar)

        binding.srvFeed.adapter = feedIssueAdapter

        setListeners()
    }

    override fun observeData() {
        super.observeData()
        lifecycleScope.launchWhenStarted {
            vm.feed.collectLatest {
                handleResource(it) { vm.getFeed(1) }
                when (it) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        feedIssueAdapter.submitList(it.success.issue)
                    }
                    is Resource.Failure -> {
                    }
                }
            }
        }
        vm.getFeed(1)
    }

    private fun setListeners() {
        requireActivity().onBackPressedDispatcher.addCallback {
            if (binding.feedCommentBox.root.isVisible) {
                binding.feedCommentBox.itUserComment.clearFocus()
                binding.feedCommentBox.root.isVisible = false
                mainActivity?.showBottomNav()
                requireView().hideKeyboard()
            }
            remove()
        }
    }

    override fun onAvatarClick(user: User) {
    }

    override fun onMoreItemClick(issue: Issue) {
    }

    override fun onLocationClick(location: Location) {
    }

    override fun onLikeClick(issue: Issue) {
    }

    override fun onDislikeClick(issue: Issue) {
    }

    override fun onShowAllCommentsClick(issue: Issue) {
    }

    override fun onCommentLikeClick(comment: Comment) {
    }

    override fun onCommentReplayClick(comment: Comment) {
    }

    override fun onCommentUserAvatarClick(user: User) {
    }

    override fun onSendCommentClick(issue: Issue) {
        mainActivity?.hideBottomNav()
        binding.feedCommentBox.root.isVisible = true
        binding.feedCommentBox.itUserComment.requestFocus()
        binding.feedCommentBox.itUserComment.showKeyboard()
    }

}