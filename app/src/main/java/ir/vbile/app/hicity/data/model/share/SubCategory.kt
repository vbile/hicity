package ir.vbile.app.hicity.data.model.share

import com.squareup.moshi.Json

data class SubCategory(
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "parentId")
    val parentId: Int
)
