package ir.vbile.app.hicity.ui.fragment.news

import dagger.hilt.android.lifecycle.*
import ir.vbile.app.hicity.base.*
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.*

@HiltViewModel
class NewsViewModel @Inject constructor(val mainCoroutineDispatcher: CoroutineDispatcher,
):BaseViewModel(mainCoroutineDispatcher) {
}