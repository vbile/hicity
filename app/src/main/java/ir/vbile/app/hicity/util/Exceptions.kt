package ir.vbile.app.hicity.util

import java.io.IOException

class NoInternetException(message: String? = null) : IOException(message)