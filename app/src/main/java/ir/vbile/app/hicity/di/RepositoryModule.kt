package ir.vbile.app.hicity.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.vbile.app.hicity.BuildConfig
import ir.vbile.app.hicity.data.remote.api.AuthApi
import ir.vbile.app.hicity.data.remote.api.FeedApi
import ir.vbile.app.hicity.data.remote.api.ProfileApi
import ir.vbile.app.hicity.data.repo.AuthRepository
import ir.vbile.app.hicity.data.repo.FeedRepository
import ir.vbile.app.hicity.data.repo.ProfileRepository
import ir.vbile.app.hicity.data.repo.dummy.DummyAuthRepository
import ir.vbile.app.hicity.data.repo.dummy.DummyFeedRepository
import ir.vbile.app.hicity.data.repo.dummy.DummyProfileRepository
import ir.vbile.app.hicity.data.repo.impl.AuthRepositoryImpl
import ir.vbile.app.hicity.data.repo.impl.FeedRepositoryImpl
import ir.vbile.app.hicity.data.repo.impl.ProfileRepositoryImpl
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Provides
    @Singleton
    internal fun provideFeedRepository(feedApi: FeedApi): FeedRepository =
        if (BuildConfig.DEMO_MODE) DummyFeedRepository() else FeedRepositoryImpl(feedApi)

    @Provides
    @Singleton
    internal fun provideAuthRepository(authApi: AuthApi): AuthRepository =
        if (BuildConfig.DEMO_MODE) DummyAuthRepository() else AuthRepositoryImpl(authApi)

    @Provides
    @Singleton
    internal fun provideProfileRepository(profileApi: ProfileApi): ProfileRepository =
        if (BuildConfig.DEMO_MODE) DummyProfileRepository() else ProfileRepositoryImpl(profileApi)
}