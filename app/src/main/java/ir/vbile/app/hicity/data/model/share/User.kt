package ir.vbile.app.hicity.data.model.share

import com.squareup.moshi.Json

data class User(
    @Json(name = "id")
    val id: Int,
    @Json(name = "fullName")
    val fullName: String,
    @Json(name = "genderId")
    val genderId: Int,
    @Json(name = "avatarUrl")
    val avatarUrl: String?,
    @Json(name = "totalIssue")
    val totalIssue: Int?,
    @Json(name = "totalVote")
    val totalVote: Int?,
    @Json(name = "totalComment")
    val totalComment: Int?,
    @Json(name = "aboutUser")
    val aboutUser: String?
)
