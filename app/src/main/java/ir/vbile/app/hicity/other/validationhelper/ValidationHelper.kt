package ir.vbile.app.hicity.other.validationhelper

import com.google.android.material.textfield.*

class ValidationHelper (private val textInputLayout: TextInputLayout) {
    var isValidate=false

    fun isRequired(text:String){
        if (text.isNullOrBlank() || text.equals("")){
            textInputLayout.error="فیلد نمی تواند خالی باشد"
            isValidate=false
        }
        isValidate=true
    }
    fun FieldIsPhone(text: String){
        if (text.length >11 || text.length< 11){
            textInputLayout.error="مقدار وارد شده صحیح نمی باشد"
            isValidate=false
        }
        isValidate=true

    }

    fun checkValidate():Boolean{
        return isValidate
    }


}