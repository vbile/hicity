package ir.vbile.app.hicity.data.model.share

import com.squareup.moshi.Json

data class Location(
    @Json(name = "locationName")
    val locationName: String,
    @Json(name = "lat")
    val lat: Double,
    @Json(name = "long")
    val long: Double
)
