package ir.vbile.app.hicity.data.repo

import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.hicity.wrapper.Resource

interface FeedRepository {
    suspend fun feed(page: Int, apiKey: String): Resource<FeedApiResponse>
}