package ir.vbile.app.hicity.util

import ir.vbile.app.hicity.BuildConfig


fun Exception.print() {
    if (BuildConfig.DEBUG)
        printStackTrace()
}

fun Throwable.print() {
    if (BuildConfig.DEBUG)
        printStackTrace()
}

