package ir.vbile.app.hicity.data.remote.responses.feed

import com.squareup.moshi.Json
import ir.vbile.app.hicity.data.model.Issue

data class FeedApiResponse(
    @Json(name = "issue")
    val issue: List<Issue>,
    @Json(name = "totalPages")
    val totalPages: Int,
    @Json(name = "pageSize")
    val pageSize: Int,
    @Json(name = "currentPage")
    val currentPage: Int
)
