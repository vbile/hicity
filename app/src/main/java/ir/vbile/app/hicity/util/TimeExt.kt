package ir.vbile.app.hicity.util

import com.ibm.icu.text.DateFormat
import com.ibm.icu.util.Calendar
import com.ibm.icu.util.ULocale
import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.data.model.share.Comment
import java.text.SimpleDateFormat
import java.util.*

private fun String.toTimeStamp(
    inputPattern: String = "yyyy-MM-dd'T'HH:mm:ss'Z'",
): Long? {
    return runCatching {
        val simpleDataFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
        val date = simpleDataFormat.parse(this)
        date ?: return null
        date.time
    }.getOrNull()
}

private fun Long.toInstaTime(multiply: Boolean = false): String {
    val dayPrefix = "روز  پیش"
    val min = 60000
    val hour = min * 60
    val aDay = hour * 24
    val towDay = aDay * 2
    val threeDay = aDay * 3
    val fourDay = aDay * 4
    val fiveDay = aDay * 5
    val sixDay = aDay * 6
    val sevenDay = aDay * 7
    val eightDay = aDay * 8

    val currentTime = System.currentTimeMillis()

    val time = if (multiply) this * 1000 else this
    val diff = currentTime - time
    return when {
        diff < aDay ->
            when {
                diff < min -> java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(diff)
                    .toString() +
                        " " + "ثانیه پیش"
                diff < hour -> java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(diff)
                    .toString() +
                        " " + "دقیقه پیش"
                else -> java.util.concurrent.TimeUnit.MILLISECONDS.toHours(diff).toString() +
                        " " + "ساعت پیش"
            }
        diff in aDay until towDay - 1 -> "1  $dayPrefix"
        diff in towDay until threeDay - 1 -> "2  $dayPrefix"
        diff in threeDay until fourDay - 1 -> "3  $dayPrefix"
        diff in fourDay until fiveDay - 1 -> "4  $dayPrefix"
        diff in fiveDay until sixDay - 1 -> "5  $dayPrefix"
        diff in sixDay until sevenDay - 1 -> "6  $dayPrefix"
        diff in sevenDay until eightDay - 1 -> "7  $dayPrefix"
        else -> this.toTime()
    }

}

private fun Long.toTime(): String {
    val date = Date(this)
    val persianLocate = ULocale("fa_IR@calendar=persian")
    val persianCalendar = Calendar.getInstance(persianLocate)
    persianCalendar.time = date
    val dateFormat: DateFormat = DateFormat.getDateInstance(DateFormat.FULL, persianLocate)
    return dateFormat.format(persianCalendar.time)
}


fun Issue.getStandardTime(): String? = cratedAt.toTimeStamp()?.toInstaTime()

fun Comment.getStandardTime(): String? = cratedAt.toTimeStamp()?.toInstaTime()
