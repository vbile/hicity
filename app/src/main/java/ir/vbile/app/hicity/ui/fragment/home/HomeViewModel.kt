package ir.vbile.app.hicity.ui.fragment.home

import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hicity.base.BaseViewModel
import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.hicity.data.repo.FeedRepository
import ir.vbile.app.hicity.di.EndPoints.fakeApiKey
import ir.vbile.app.hicity.wrapper.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    val mainCoroutineDispatcher: CoroutineDispatcher,
    private val feedRepositoryInterface: FeedRepository
) : BaseViewModel(mainCoroutineDispatcher) {

    private val feedChanel = Channel<Resource<FeedApiResponse>>()
    val feed = feedChanel.receiveAsFlow()
    fun getFeed(page: Int) = doInMain {
        feedChanel.send(Resource.Loading)
        feedChanel.send(feedRepositoryInterface.feed(page, fakeApiKey))
    }

}