package ir.vbile.app.hicity.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

abstract class BaseViewModel(private val mainCoroutineDispatcher: CoroutineDispatcher) :
    ViewModel() {

    fun doInMain(action: suspend () -> Unit) =
        viewModelScope.launch(mainCoroutineDispatcher) { action.invoke() }
}