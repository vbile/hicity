package ir.vbile.app.hicity.data.repo.dummy

import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.profile.ResponseFeedApiProfile
import ir.vbile.app.hicity.data.repo.ProfileRepository
import ir.vbile.app.hicity.data.factory.DummyProfileApiResponse
import ir.vbile.app.hicity.wrapper.*

class DummyProfileRepository : ProfileRepository {
    override suspend fun getUser(userId: Int): Resource<User> =
        Resource.Success(DummyProfileApiResponse.getUser(userId))

    override suspend fun getUserFeedProfile(
        page: Int,
        apiKey: String
    ): Resource<ResponseFeedApiProfile> =
        Resource.Success(DummyProfileApiResponse.dummyFeedApiResponse)


}