package ir.vbile.app.hicity.data.factory

import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse

/**
 *Use for testing or dummy launch
 * */
object DummyObjects {
    val dummyUser =
        User(
            1,
            "پژمان رنجبر",
            1,
            "https://avatars.githubusercontent.com/u/40461630",
            10,
            150,
            10,
            ""
        )
    val dummyUser2 =
        User(
            2, "وحید گروسی", 1, "https://avatars.githubusercontent.com/u/19606714",
            10,
            150,
            10,
            ""
        )
    val dummyUser3 =
        User(
            3, "اصغر حسینی", 1, "https://avatars.githubusercontent.com/u/29679706",
            10,
            150,
            10,
            ""
        )

    val dummyTime = "2021-07-02T10:50:41Z"
    val dummyLocation = Location("البرز،نظرآباد", 35.9726, 50.6068)
    val issuePhotos = listOf(
        "https://www.dana.ir/content/files/Parsiankhabar/1070464.jpg",
        "https://www.dana.ir/content/files/Parsiankhabar/1070465.jpg",
        "https://www.dana.ir/content/files/Parsiankhabar/1070467.jpg"
    )
    val dummyCategory = Category(1, "راه و شهرسازی")
    val dummySubCategory = SubCategory(1, " لکه گیری چاله های معابر", 1)
    val dummyDescription =
        "کوچه\u200Cها و خیابان\u200Cهای پر چاله\u200Cچوله نه\u200Cتنها موجب آزار و اذیت شهروندان می\u200Cشود بلکه زیبنده شهر نیست و اگر مسافر و یا گردشگری از شهر عبور کند در نگاه اول با خیابان\u200Cهای فرسوده مواجه می\u200Cشود."

    val dummyComments = listOf(
        Comment(
            1, dummyUser, "این یک کامنت است 1",
            isPublished = true,
            isApproved = true,
            cratedAt = dummyTime,
            isLiked = true,
            likeCount = 100
        ),
        Comment(
            2, dummyUser2, "این یک کامنت است 2",
            isPublished = true,
            isApproved = true,
            cratedAt = dummyTime,
            isLiked = false,
            likeCount = 50
        ),
        Comment(
            3, dummyUser3, "این یک کامنت است 3",
            isPublished = true,
            isApproved = true,
            cratedAt = dummyTime,
            isLiked = false,
            likeCount = 0
        )
    )
    val dummyIssues = listOf(dummyUser, dummyUser2, dummyUser3).mapIndexed { index, user ->
        Issue(
            index,
            user,
            dummyTime,
            dummyLocation,
            issuePhotos,
            true,
            dummyCategory,
            dummySubCategory,
            dummyDescription,
            dummyComments
        )
    }

    val dummyFeedApiResponse = FeedApiResponse(dummyIssues, 1, 5, 1)

}