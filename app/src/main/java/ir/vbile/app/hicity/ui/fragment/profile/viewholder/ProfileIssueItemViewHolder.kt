package ir.vbile.app.hicity.ui.fragment.profile.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.databinding.ItemProfileIssueBinding
import ir.vbile.app.hicity.util.getStandardTime

class ProfileIssueItemViewHolder(val itemProfileIssueBinding: ItemProfileIssueBinding) :
    RecyclerView.ViewHolder(itemProfileIssueBinding.root) {
    fun bind(issue: Issue) {
        with(itemProfileIssueBinding) {
            Glide.with(this.root).load(issue.photosUrl[0]).into(itemImage)
            itemTitle.text = issue.user.fullName
            itemTime.text = issue.getStandardTime()
            itemCategory.text = root.context.getString(R.string.issue_category_with_sbu_category_title,issue.category.name,issue.subCategory.name)
            itemDescription.text = issue.description
        }
    }
    companion object {
        fun getInstance(viewGroup: ViewGroup): ProfileIssueItemViewHolder {
            val context = viewGroup.context
            val inflater = LayoutInflater.from(context)
            val itemProfileIssueBinding =
                ItemProfileIssueBinding.inflate(inflater, viewGroup, false)
            return ProfileIssueItemViewHolder(itemProfileIssueBinding)
        }
    }
}