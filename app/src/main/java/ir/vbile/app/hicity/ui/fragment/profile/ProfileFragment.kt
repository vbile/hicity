package ir.vbile.app.hicity.ui.fragment.profile

import androidx.lifecycle.*
import androidx.recyclerview.widget.*
import com.bumptech.glide.*
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment
import ir.vbile.app.hicity.data.model.*
import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.databinding.*
import ir.vbile.app.hicity.other.*
import ir.vbile.app.hicity.ui.fragment.home.adapter.*
import ir.vbile.app.hicity.ui.fragment.profile.adapter.*
import ir.vbile.app.hicity.ui.fragment.profile.listener.*
import ir.vbile.app.hicity.wrapper.*
import kotlinx.coroutines.flow.*
import javax.inject.*

@AndroidEntryPoint
class ProfileFragment :
    BaseFragment<ProfileViewModel>(R.layout.fragment_profile, ProfileViewModel::class),
    ProfileIssueClickListener {
    private val binding by viewBinding(FragmentProfileBinding::bind)
    private val profileIssueAdapter by lazy { ProfileIssueAdapter(this) }

    @Inject
    lateinit var glide: RequestManager
    override fun setUpViews() {
        super.setUpViews()
        binding.ivSetting.setOnClickListener { }
        binding.ivProfileImage.setOnClickListener { }
        binding.ivEdit.setOnClickListener { }
        setUpIssuesAdapter()
    }

    private fun setUpIssuesAdapter() {
        profileIssueAdapter.setOnItemClickListener { }
        binding.rvProfile.apply {
            adapter = profileIssueAdapter
        }
    }

    override fun observeData() {
        super.observeData()
        subscribeToObserveUser()
        subscribeToObserveFeedProfile()
    }

    private fun subscribeToObserveFeedProfile() {
        lifecycleScope.launchWhenStarted {
            vm.feedEvent.collect { event ->
                handleResource(event) { vm.getFeedProfile(1) }
                when (event) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        profileIssueAdapter.submitList(event.success.issue)
                    }
                    is Resource.Failure -> {
                    }
                }
            }
        }
    }

    private fun subscribeToObserveUser() {
        lifecycleScope.launchWhenStarted {
            vm.userEvent.collect { event ->
                handleResource(event){vm.getUser(1001)}
                when (event) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        setUserProfile(event.success)
                    }
                    is Resource.Failure -> {
                    }
                }
            }
        }
    }

    private fun setUserProfile(user: User) {
        glide.load(user.avatarUrl).into(binding.ivProfileImage)
        binding.tvUserName.text = user.fullName
        binding.tvUserId.text = user.genderId.toString()
        binding.tvUserAttributes.text = "قانونمند"
        binding.tvUserComment.text = user.totalComment.toString()
        binding.tvUserVote.text = user.totalVote.toString()
        binding.tvUserReport.text = user.totalIssue.toString()
        binding.tvUserDetail.text = user.aboutUser
    }

    override fun onCheckTheStatusClick(issue: Issue) {
    }
}