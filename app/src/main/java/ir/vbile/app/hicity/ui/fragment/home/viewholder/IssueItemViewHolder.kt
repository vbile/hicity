package ir.vbile.app.hicity.ui.fragment.home.viewholder

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.databinding.IssueItemBinding
import ir.vbile.app.hicity.ui.fragment.home.adapter.IssueMediaAdapter
import ir.vbile.app.hicity.util.getStandardTime

class IssueItemViewHolder(val issueItemBinding: IssueItemBinding) :
    RecyclerView.ViewHolder(issueItemBinding.root) {
    private val issueMediaAdapter by lazy { IssueMediaAdapter() }

    @SuppressLint("SetTextI18n")
    fun bind(currentUserAvatarUrl: String, issue: Issue) {
        with(issueItemBinding) {
            vpIssueMedia.adapter = issueMediaAdapter

            Glide.with(this.root).load(issue.user.avatarUrl).into(ivAvatar)
            tvUserName.text = issue.user.fullName
            tvTime.text = issue.getStandardTime()
            cvIssueCount.isVisible = issue.photosUrl.size > 1
            issueMediaAdapter.submitList(issue.photosUrl)
            btnLocation.text = issue.location.locationName

            tvCategory.text = "${issue.category.name}-${issue.subCategory.name}"
            tvDescription.text = issue.description
            Glide.with(this.root).load(currentUserAvatarUrl).into(ivUserAvatar)
            if (issue.comments.isNotEmpty())
                issue.comments.first().let {
                    Glide.with(this.root).load(it.user.avatarUrl)
                        .into(previewComment.ivCommentAvatar)
                    previewComment.tvComment.text = it.Comment
                    previewComment.tvCommentTime.text = it.getStandardTime()
                    previewComment.likeCount.text = it.likeCount.toString()
                }
            else
                previewComment.root.isVisible = false
        }


    }

    companion object {
        fun getInstance(viewGroup: ViewGroup): IssueItemViewHolder {
            val context = viewGroup.context
            val inflater = LayoutInflater.from(context)
            val issueItemBinding =
                IssueItemBinding.inflate(inflater, viewGroup, false)
            return IssueItemViewHolder(issueItemBinding)
        }
    }
}