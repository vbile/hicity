package ir.vbile.app.hicity.data.repo

import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.profile.ResponseFeedApiProfile
import ir.vbile.app.hicity.wrapper.*

interface ProfileRepository {
    suspend fun getUser(userId: Int): Resource<User>
    suspend fun getUserFeedProfile(page: Int, apiKey: String): Resource<ResponseFeedApiProfile>
}