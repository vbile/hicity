package ir.vbile.app.hicity.ui.fragment.authentication.verificationcode

import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hicity.base.BaseViewModel
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@HiltViewModel
class VerificationCodeViewModel @Inject constructor(
    val mainCoroutineDispatcher: CoroutineDispatcher,
) : BaseViewModel(mainCoroutineDispatcher) {
}