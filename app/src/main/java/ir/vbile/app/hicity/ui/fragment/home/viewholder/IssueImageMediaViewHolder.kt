package ir.vbile.app.hicity.ui.fragment.home.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.vbile.app.hicity.databinding.IssueImageMediaItemBinding

class IssueImageMediaViewHolder(private val issueImageMediaItemBinding: IssueImageMediaItemBinding) :
    RecyclerView.ViewHolder(issueImageMediaItemBinding.root) {

    fun bind(imageUrl: String) {
        with(issueImageMediaItemBinding) {
            Glide.with(this.root).load(imageUrl).into(ivIssueImage)
        }
    }

    companion object {
        fun getInstance(viewGroup: ViewGroup): IssueImageMediaViewHolder {
            val context = viewGroup.context
            val inflater = LayoutInflater.from(context)
            val issueImageMediaItemBinding =
                IssueImageMediaItemBinding.inflate(inflater, viewGroup, false)
            return IssueImageMediaViewHolder(issueImageMediaItemBinding)
        }
    }
}