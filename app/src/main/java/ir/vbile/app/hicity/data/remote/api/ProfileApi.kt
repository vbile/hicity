package ir.vbile.app.hicity.data.remote.api

import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.profile.ResponseFeedApiProfile
import ir.vbile.app.hicity.di.*
import retrofit2.*
import retrofit2.http.*

interface ProfileApi {
    @POST(EndPoints.profileRequest)
    @FormUrlEncoded
    suspend fun getUser(
        @Field("userId")
        userId: Int
    ): Response<User>

    @POST(EndPoints.profileFeedRequest)
    @FormUrlEncoded
    suspend fun getProfileFeed(
        @Field("page")
        page: Int,
        @Field("apiKey")
        apiKey: String,
        @Field("itemsCountPerPage")
        pageSize: Int = EndPoints.itemsCountPerPage
    ): Response<ResponseFeedApiProfile>
}