package ir.vbile.app.hicity.ui.fragment.home.listener

import ir.vbile.app.hicity.data.model.Issue
import ir.vbile.app.hicity.data.model.share.Comment
import ir.vbile.app.hicity.data.model.share.Location
import ir.vbile.app.hicity.data.model.share.User

interface IssueClickListener {
    fun onAvatarClick(user: User)
    fun onMoreItemClick(issue: Issue)
    fun onLocationClick(location: Location)
    fun onLikeClick(issue: Issue)
    fun onDislikeClick(issue: Issue)
    fun onShowAllCommentsClick(issue: Issue)
    fun onCommentLikeClick(comment: Comment)
    fun onCommentReplayClick(comment: Comment)
    fun onCommentUserAvatarClick(user: User)
    fun onSendCommentClick(issue: Issue)
}