package ir.vbile.app.hicity.ui.fragment.profile.listener
import ir.vbile.app.hicity.data.model.Issue

interface ProfileIssueClickListener {
    fun onCheckTheStatusClick(issue: Issue)
}