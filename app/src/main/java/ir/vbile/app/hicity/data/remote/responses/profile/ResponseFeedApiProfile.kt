package ir.vbile.app.hicity.data.remote.responses.profile

import com.squareup.moshi.*
import ir.vbile.app.hicity.data.model.*

data class ResponseFeedApiProfile(
    @Json(name = "issue")
    val issue: List<Issue>,
    @Json(name = "totalPages")
    val totalPages: Int,
    @Json(name = "pageSize")
    val pageSize: Int,
    @Json(name = "currentPage")
    val currentPage: Int
)
