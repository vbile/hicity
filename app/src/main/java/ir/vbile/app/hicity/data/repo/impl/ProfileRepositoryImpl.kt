package ir.vbile.app.hicity.data.repo.impl

import ir.vbile.app.hicity.data.remote.api.ProfileApi
import ir.vbile.app.hicity.data.model.share.*
import ir.vbile.app.hicity.data.remote.responses.profile.ResponseFeedApiProfile
import ir.vbile.app.hicity.data.repo.ProfileRepository
import ir.vbile.app.hicity.util.*
import ir.vbile.app.hicity.wrapper.*

class ProfileRepositoryImpl(private val profileApi: ProfileApi) : ProfileRepository {
    override suspend fun getUser(userId: Int): Resource<User> =
        safeApiCall { profileApi.getUser(userId) }

    override suspend fun getUserFeedProfile(
        page: Int,
        apiKey: String
    ): Resource<ResponseFeedApiProfile> =
        safeApiCall { profileApi.getProfileFeed(page, apiKey) }
}