package ir.vbile.app.hicity.ui.fragment.authentication.login

import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.base.BaseFragment
import ir.vbile.app.hicity.databinding.FragmentLoginBinding
import ir.vbile.app.hicity.extentions.afterTextChanged
import ir.vbile.app.hicity.extentions.longToast
import ir.vbile.app.hicity.other.viewBinding
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class LoginFragment : BaseFragment<LoginViewModel>(R.layout.fragment_login, LoginViewModel::class) {
    private val binding by viewBinding(FragmentLoginBinding::bind)
    override fun setUpViews() {
        super.setUpViews()
        binding.etMobile.afterTextChanged { it ->
            vm.mobileNumber.value = it
            binding.btnAccept.isEnabled = it.count { it.isDigit() } == 11
        }
        binding.btnAccept.setOnClickListener {
            vm.validateMobileNumber()
        }
    }

    override fun observeData() {
        super.observeData()
        lifecycleScope.launchWhenStarted {
            vm.loginEvent.collectLatest { event ->
                when (event) {
                    LoginViewModel.LoginEvent.InvalidPhoneNumber -> {
                        binding.etMobile.error =
                            getString(R.string.validation_mobile_number_is_invalid)
                        longToast(getString(R.string.validation_mobile_number_is_invalid))
                    }
                    LoginViewModel.LoginEvent.MobileNumberIsEmpty -> {
                        binding.etMobile.error =
                            getString(R.string.validation_mobile_number_is_empty)
                        longToast(getString(R.string.validation_mobile_number_is_empty))
                    }
                    is LoginViewModel.LoginEvent.ShowError -> {
                    }
                    is LoginViewModel.LoginEvent.NavigateToVerifyOtpScreen -> {
                        longToast(getString(R.string.fragment_login_otp_sent))
                    }
                }
            }
        }
    }
}