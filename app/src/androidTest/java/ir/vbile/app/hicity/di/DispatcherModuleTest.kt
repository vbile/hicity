package ir.vbile.app.hicity.di

import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import javax.inject.Singleton

@TestInstallIn(components = [SingletonComponent::class], replaces = [DispatcherModule::class])
@Module
@OptIn(ExperimentalCoroutinesApi::class)
object DispatcherModuleTest {
    @Singleton
    @Provides
    fun provideMainDispatcher(): CoroutineDispatcher = TestCoroutineDispatcher()
}