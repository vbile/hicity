package ir.vbile.app.hicity.ui.fragment.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.squareup.moshi.Moshi
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import ir.vbile.app.hicity.data.factory.DummyObjects.dummyFeedApiResponse
import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.hicity.data.repo.FeedRepository
import ir.vbile.app.hicity.wrapper.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import mockwebserver3.MockResponse
import mockwebserver3.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
@OptIn(ExperimentalCoroutinesApi::class)
class HomeViewModelTest {
    @get:Rule
    val hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var feedRepository: FeedRepository

    @Inject
    lateinit var mockWebServer: MockWebServer

    @Inject
    lateinit var moshi: Moshi

    @Inject
    lateinit var mainCoroutineDispatcher: CoroutineDispatcher

    private lateinit var homeViewModel: HomeViewModel
    private val jsonAdapter get() = moshi.adapter(FeedApiResponse::class.java)

    @Before
    fun setUp() {
        hiltAndroidRule.inject()
        homeViewModel = HomeViewModel(mainCoroutineDispatcher, feedRepository)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
        (mainCoroutineDispatcher as TestCoroutineDispatcher).cleanupTestCoroutines()
    }

    private fun enqueueSuccessResponse() = runBlocking {
        mockWebServer.start(port = 57594)
        mockWebServer.enqueue(
            MockResponse().setResponseCode(200)
                .setBody(jsonAdapter.toJson(dummyFeedApiResponse))
        )
    }

    /**
     * When call getFeed() then api return successfully data's,should feed flow return success.
     * */
    @Test
    fun test1() = runBlocking {
        enqueueSuccessResponse()
        homeViewModel.getFeed(1)
        val resource = homeViewModel.feed.take(2).toList()
        assertThat(resource[0] is Resource.Loading).isTrue()
        assertThat(resource[1].success).isEqualTo(dummyFeedApiResponse)
    }
}