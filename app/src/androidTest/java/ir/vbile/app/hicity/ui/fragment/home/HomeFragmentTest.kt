package ir.vbile.app.hicity.ui.fragment.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.google.common.truth.Truth.assertThat
import com.squareup.moshi.Moshi
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import ir.vbile.app.getElementFromMatchAtPosition
import ir.vbile.app.hicity.FakeNetworkConnectionInterceptor
import ir.vbile.app.hicity.R
import ir.vbile.app.hicity.data.factory.DummyObjects
import ir.vbile.app.hicity.data.factory.DummyObjects.dummyIssues
import ir.vbile.app.hicity.data.remote.responses.feed.FeedApiResponse
import ir.vbile.app.isSoftKeyboardShown
import ir.vbile.app.launchFragmentInHiltContainer
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import mockwebserver3.MockResponse
import mockwebserver3.MockWebServer
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
@OptIn(ExperimentalCoroutinesApi::class)
class HomeFragmentTest {
    @get:Rule
    val hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var mockWebServer: MockWebServer

    @Inject
    lateinit var moshi: Moshi

    @Inject
    lateinit var fakeNetworkConnectionInterceptor: FakeNetworkConnectionInterceptor

    @Inject
    lateinit var mainCoroutineDispatcher: CoroutineDispatcher

    private val feedApiResponseJsonAdapter get() = moshi.adapter(FeedApiResponse::class.java)


    @Before
    fun setUp() {
        hiltAndroidRule.inject()

    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
        (mainCoroutineDispatcher as TestCoroutineDispatcher).cleanupTestCoroutines()
    }

    private fun enqueueSuccessResponse()= runBlocking {
        mockWebServer.start(port = 57594)
        mockWebServer.enqueue(
            MockResponse().setResponseCode(200)
                .setBody(feedApiResponseJsonAdapter.toJson(DummyObjects.dummyFeedApiResponse))
        )
    }

    /**
     * make sure recycler view showing issues or not
     * */
    @Test
    fun test1() {
        enqueueSuccessResponse()
        launchFragmentInHiltContainer<HomeFragment>()
        onView(withText(dummyIssues.first().user.fullName)).check(matches(isDisplayed()))

    }

    /**
     * make sure feed_comment_box work fine
     * */
    @Test
    fun test2() {
        enqueueSuccessResponse()
        launchFragmentInHiltContainer<HomeFragment>()
        onView(getElementFromMatchAtPosition(withId(R.id.iv_user_comment), 0)).perform(click())

        //check keyboard is showing
        assertThat(isSoftKeyboardShown()).isTrue()

        //check comment_box is showing
        onView(withId(R.id.feed_comment_box)).check(matches(isDisplayed()))

        //press back again should comment_box gone
        pressBack()
        pressBack()
        onView(withId(R.id.feed_comment_box)).check(matches(not(isDisplayed())))

    }

    /**
     * when no internet failure came from api should show no internet layout
     * */
    @Test
    fun test3() {
        //turn off internet
        fakeNetworkConnectionInterceptor.isInternetAvailable = false

        launchFragmentInHiltContainer<HomeFragment>()
        onView(withId(R.id.btn_retry)).check(matches(isDisplayed()))
    }

    /**
     * when btn retry clicked should no internet layout gone and show issues
     * */
    @Test
    fun test4() {

        //turn off internet
        fakeNetworkConnectionInterceptor.isInternetAvailable = false

        launchFragmentInHiltContainer<HomeFragment>()
        onView(withId(R.id.btn_retry)).check(matches(isDisplayed()))

        //turn on internet
        fakeNetworkConnectionInterceptor.isInternetAvailable = true
        enqueueSuccessResponse()

        onView(withId(R.id.btn_retry)).perform(click())

        onView(withText(dummyIssues.first().user.fullName)).check(matches(isDisplayed()))
    }
}
