package ir.vbile.app.hicity.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import ir.vbile.app.hicity.FakeNetworkConnectionInterceptor
import ir.vbile.app.hicity.data.remote.api.AuthApi
import ir.vbile.app.hicity.data.remote.api.FeedApi
import ir.vbile.app.hicity.data.remote.api.ProfileApi
import mockwebserver3.MockWebServer
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@TestInstallIn(components = [SingletonComponent::class], replaces = [RestClientModule::class])
@Module
object RestClientModuleTest {
    @Provides
    @Singleton
    internal fun provideMockFeedApiService(retrofit: Retrofit): FeedApi =
        retrofit.create(FeedApi::class.java)

    @Provides
    @Singleton
    internal fun provideMockAuthApiService(retrofit: Retrofit): AuthApi =
        retrofit.create(AuthApi::class.java)

    @Provides
    @Singleton
    internal fun provideMockProfileApiService(retrofit: Retrofit): ProfileApi =
        retrofit.create(ProfileApi::class.java)

    @Provides
    @Singleton
    internal fun provideMockHiCityRetrofit(
        okHttpClient: OkHttpClient,
        moshi: Moshi,
        @Named("internetClient") fakeNetworkConnectionInterceptorClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .baseUrl("http://localhost:57594/")
        .client(okHttpClient)
        .client(fakeNetworkConnectionInterceptorClient)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    @Provides
    @Singleton
    internal fun provideMockWebServer(): MockWebServer = MockWebServer()

    @Provides
    @Singleton
    @Named("internetClient")
    internal fun provideFakeNetworkConnectionInterceptorClient(fakeNetworkConnectionInterceptor: FakeNetworkConnectionInterceptor): OkHttpClient =
        OkHttpClient.Builder().addInterceptor(fakeNetworkConnectionInterceptor).build()

    @Provides
    @Singleton
    internal fun provideFakeNetworkConnectionInterceptor(): FakeNetworkConnectionInterceptor =
        FakeNetworkConnectionInterceptor()


}