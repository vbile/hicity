package ir.vbile.app.hicity.di

import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import ir.vbile.app.hicity.data.remote.api.AuthApi
import ir.vbile.app.hicity.data.remote.api.FeedApi
import ir.vbile.app.hicity.data.remote.api.ProfileApi
import ir.vbile.app.hicity.data.repo.AuthRepository
import ir.vbile.app.hicity.data.repo.FeedRepository
import ir.vbile.app.hicity.data.repo.ProfileRepository
import ir.vbile.app.hicity.data.repo.impl.AuthRepositoryImpl
import ir.vbile.app.hicity.data.repo.impl.FeedRepositoryImpl
import ir.vbile.app.hicity.data.repo.impl.ProfileRepositoryImpl
import javax.inject.Singleton

@TestInstallIn(components = [SingletonComponent::class], replaces = [RepositoryModule::class])
@Module
object RepositoryModuleTest {
    @Provides
    @Singleton
    internal fun provideFeedRepository(feedApi: FeedApi): FeedRepository =
        FeedRepositoryImpl(feedApi)

    @Provides
    @Singleton
    internal fun provideAuthRepository(authApi: AuthApi): AuthRepository =
        AuthRepositoryImpl(authApi)

    @Provides
    @Singleton
    internal fun provideProfileRepository(profileApi: ProfileApi): ProfileRepository =
        ProfileRepositoryImpl(profileApi)
}